package com.kchadaj.whatstheweather.v1;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class ForecastParser {
    private static final ForecastParser instance = new ForecastParser();

    static ForecastParser getInstance() {
        return instance;
    }

    ForecastData parse(String requestResult) {
        ForecastData data = new ForecastData(new City(), 1);
        try {
            JSONObject request = new JSONObject(requestResult);
            if (isValidRequestResult(request)) {
                int count = getForecastsNumber(request);
                data = new ForecastData(parseCity(request), count);

                JSONArray array = request.getJSONArray("list");
                for (int i = 0; i < count; i++) {
                    JSONObject jsonForecast = array.getJSONObject(i);
                    data.getForecasts().set(i, parseForecast(jsonForecast));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    private static Boolean isValidRequestResult(JSONObject data) throws JSONException {
        return "200".equals(data.getString("cod"));
    }

    private static int getForecastsNumber(JSONObject data) throws JSONException {
        return data.getInt("cnt");
    }

    private City parseCity(JSONObject request) throws JSONException {
        JSONObject jsonCity = request.getJSONObject("city");
        String name = jsonCity.getString("name");
        String countryCode = jsonCity.getString("country");

        JSONObject coordinates = jsonCity.getJSONObject("coord");
        Double latitude = coordinates.getDouble("lat");
        Double longitude = coordinates.getDouble("lon");

        return new City(name, countryCode, latitude, longitude);
    }

    private Forecast parseForecast(JSONObject request) throws JSONException {
        String[] dt = request.getString("dt_txt").split(" ");
        String date = dt[0];
        String time = dt[1].substring(0, dt[1].length() - 3);

        JSONObject main = request.getJSONObject("main");
        Double temperature = convertToCelsius(main.getDouble("temp"));
        Double pressure = main.getDouble("pressure");

        Double wind = parseWeatherCondition(request, "wind", "speed");
        Double rain = parseWeatherCondition(request, "rain", "3h");
        Double snow = parseWeatherCondition(request, "snow", "3h");

        String description = parseDescription(request);

        return new Forecast(date, time, description, temperature, pressure, wind, rain, snow);
    }

    private Double convertToCelsius(Double kelvin) {
        return (kelvin - 273.15);
    }

    private Double parseWeatherCondition(JSONObject jsonObject, String condition, String tag) throws JSONException {
        if (jsonObject.has(condition)) {
            JSONObject json = jsonObject.getJSONObject(condition);
            if (json.length() > 0) {
                return json.getDouble(tag);
            }
        }
        return 0.0d;
    }

    private String parseDescription(JSONObject json) throws JSONException {
        JSONArray weatherJSON = json.getJSONArray("weather");
        if (weatherJSON.length() > 0) {
            JSONObject weather = weatherJSON.getJSONObject(0);
            return weather.getString("description");
        }
        return "";
    }
}
