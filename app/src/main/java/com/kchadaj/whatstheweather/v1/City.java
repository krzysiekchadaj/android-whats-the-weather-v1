package com.kchadaj.whatstheweather.v1;

class City {
    private String name;
    private String countryCode;
    private Double latitude;
    private Double longitude;
    private Boolean isBlank;

    City() {
        name = "";
        countryCode = "";
        latitude = 0.0d;
        longitude = 0.0d;
        isBlank = true;
    }

    City(String name, String countryCode, Double latitude, Double longitude) {
        this.name = name;
        this.countryCode = countryCode;
        this.latitude = latitude;
        this.longitude = longitude;
        isBlank = false;
    }

    String printCity() {
        if (!isBlank)
            return name + ", " + countryCode;
        return "";
    }

    String printCoordinates() {
        if (!isBlank) {
            StringBuilder sb = new StringBuilder();
            sb.append(Double.toString(latitude));
            if (latitude > 0)
                sb.append("N");
            else
                sb.append("S");
            sb.append(" ");
            sb.append(Double.toString(longitude));
            if (longitude > 0)
                sb.append("E");
            else
                sb.append("W");
            return sb.toString();
        }
        return "";
    }
}
