package com.kchadaj.whatstheweather.v1;

import java.util.ArrayList;
import java.util.Collections;

class ForecastData {
    private City city;
    private ArrayList<Forecast> forecasts;

    ForecastData(City city, int count) {
        this.city = city;
        this.forecasts = new ArrayList<>(Collections.nCopies(count, new Forecast()));
    }

    City getCity() {
        return city;
    }

    ArrayList<Forecast> getForecasts() {
        return forecasts;
    }
}
