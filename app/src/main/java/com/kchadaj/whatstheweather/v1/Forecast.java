package com.kchadaj.whatstheweather.v1;

import java.util.Locale;

class Forecast {
    private String date;
    private String time;
    private String description;

    private Double temperature;
    private Double pressure;
    private Double windSpeed;
    private Double rainRate;
    private Double snowRate;

    private Boolean isBlank;

    Forecast() {
        date = "";
        time = "";
        description = "";

        temperature = 0.0d;
        pressure = 0.0d;
        windSpeed = 0.0d;
        rainRate = 0.0d;
        snowRate = 0.0d;

        isBlank = true;
    }

    Forecast(String date, String time, String description, Double temperature, Double pressure,
             Double windSpeed, Double rainRate, Double snowRate) {
        this.date = date;
        this.time = time;
        this.description = description;

        this.temperature = temperature;
        this.pressure = pressure;
        this.windSpeed = windSpeed;
        this.rainRate = rainRate;
        this.snowRate = snowRate;

        this.isBlank = false;
    }

    String printDate() {
        if (!isBlank)
            return date;
        return "";
    }

    String printTime() {
        if (!isBlank)
            return time;
        return "";
    }

    String printDescription() {
        if (!isBlank)
            return description.toUpperCase();
        return "";
    }

    String printTemperature() {
        if (!isBlank)
            return String.format(Locale.getDefault(), "% 2d", Math.round(temperature));
        return "";
    }

    String printPressure() {
        if (!isBlank)
            return String.format(Locale.getDefault(),"%4.0f", pressure);
        return "";
    }

    String printWindSpeed() {
        if (!isBlank)
            return String.format(Locale.getDefault(), "%2.0f", windSpeed);
        return "";
    }

    String printRainRate() {
        if (!isBlank)
            return String.format(Locale.getDefault(), "%3.1f", rainRate);
        return "";
    }

    String printSnowRate() {
        if (!isBlank)
            return String.format(Locale.getDefault(), "%3.1f", snowRate);
        return "";
    }
}
