package com.kchadaj.whatstheweather.v1;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class WeatherActivity extends AppCompatActivity {

    private static final String API_ADDRESS = "http://api.openweathermap.org/data/2.5/forecast";
    private static final String API_KEY = "--->to be replaced with my key to this API<---";

    private static final int PERMISSION_REQUEST_ACCESS_COARSE_LOCATION = 101;

    private EditText cityEditText;
    private ImageButton locationButton;
    private TextView coordinatesTextView;

    private TextView overallTextView, overallDescriptionTextView;
    private TextView temperatureTextView, temperatureDescriptionTextView;
    private TextView windTextView, windDescriptionTextView;
    private TextView rainTextView, rainDescriptionTextView;
    private TextView pressureTextView, pressureDescriptionTextView;
    private TextView snowTextView, snowDescriptionTextView;

    private TextView dateTextView, timeTextView;
    private SeekBar timeSpanSeekBar;

    private ForecastData forecastData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        findViews();
    }

    private void findViews() {
        cityEditText = findViewById(R.id.cityEditText);
        locationButton = findViewById(R.id.locationButton);
        coordinatesTextView = findViewById(R.id.coordinatesTextView);

        overallTextView = findViewById(R.id.overallTextView);
        temperatureTextView = findViewById(R.id.temperatureTextView);
        overallDescriptionTextView = findViewById(R.id.overallDescriptionTextView);
        temperatureDescriptionTextView = findViewById(R.id.temperatureDescriptionTextView);

        windTextView = findViewById(R.id.windTextView);
        rainTextView = findViewById(R.id.rainTextView);
        windDescriptionTextView = findViewById(R.id.windDescriptionTextView);
        rainDescriptionTextView = findViewById(R.id.rainDescriptionTextView);

        pressureTextView = findViewById(R.id.pressureTextView);
        snowTextView = findViewById(R.id.snowTextView);
        pressureDescriptionTextView = findViewById(R.id.pressureDescriptionTextView);
        snowDescriptionTextView = findViewById(R.id.snowDescriptionTextView);

        dateTextView = findViewById(R.id.dateTextView);
        timeTextView = findViewById(R.id.timeTextView);
        timeSpanSeekBar = findViewById(R.id.timespanSeekBar);
    }

    @Override
    protected void onResume() {
        super.onResume();

        setListeners();
    }

    private void setListeners() {
        setCityEditTextListeners();
        setLocationButtonListeners();
        setTimeSpanSeekBarListeners();
    }

    private void setCityEditTextListeners() {
        cityEditText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (isTextEditDone(actionId, event)) {
                    requestWeatherForecastByCity();
                    return true;
                }
                return false;
            }
        });
    }

    private boolean isTextEditDone(int actionId, KeyEvent event) {
        return actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE ||
                (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER);
    }

    private void requestWeatherForecastByCity() {
        String city = cityEditText.getText().toString();
        if (!city.isEmpty()) {
            try {
                String cityQuery = URLEncoder.encode(city, "UTF-8");
                String address = constructAddressByCity(cityQuery);
                new ForecastRequest(this).execute(address);
            } catch (UnsupportedEncodingException e) {
                Toast.makeText(getApplicationContext(), getString(R.string.toast_city_not_valid), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.toast_city_not_valid), Toast.LENGTH_SHORT).show();
        }
    }

    private String constructAddressByCity(String cityQuery) {
        return API_ADDRESS + "?q=" + cityQuery + "&APPID=" + API_KEY;
    }

    private void setLocationButtonListeners() {
        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestLocationPermission();
            }
        });
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                PERMISSION_REQUEST_ACCESS_COARSE_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_REQUEST_ACCESS_COARSE_LOCATION:
                if (isLocationPermitted(grantResults)) {
                    requestLocation();
                }
                break;
        }
    }

    private boolean isLocationPermitted(int[] grantResults) {
        if (grantResults.length > 0) {
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private void requestLocation() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            if (locationManager != null) {
                Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    requestForecastByLocation(location);
                } else {
                    locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, new LocationListener() {
                        @Override
                        public void onLocationChanged(Location location) {
                            requestForecastByLocation(location);
                        }

                        @Override
                        public void onStatusChanged(String provider, int status, Bundle extras) {
                            // empty
                        }

                        @Override
                        public void onProviderEnabled(String provider) {
                            requestLocation();
                        }

                        @Override
                        public void onProviderDisabled(String provider) {
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_gps_disabled), Toast.LENGTH_SHORT).show();
                        }
                    }, null);
                }
            }
            return;
        }
        Toast.makeText(getApplicationContext(),getString(R.string.toast_gps_error), Toast.LENGTH_SHORT).show();
    }

    private void requestForecastByLocation(Location location) {
        String address = constructAddressByLocation(location);
        new ForecastRequest(WeatherActivity.this).execute(address);
    }

    private String constructAddressByLocation(Location location) {
        return API_ADDRESS + "?lat=" + String.valueOf(location.getLatitude())
                + "&lon=" + String.valueOf(location.getLongitude()) + "&APPID=" + API_KEY;
    }

    private void setTimeSpanSeekBarListeners() {
        timeSpanSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                showForecast(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // empty
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // empty
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        removeListeners();
    }

    private void removeListeners() {
        removeCityEditTextListeners();
        removeLocationButtonListeners();
        removeTimeSpanSeekBarListeners();
    }

    private void removeCityEditTextListeners() {
        cityEditText.setOnEditorActionListener(null);
    }

    private void removeLocationButtonListeners() {
        locationButton.setOnClickListener(null);
    }

    private void removeTimeSpanSeekBarListeners() {
        timeSpanSeekBar.setOnSeekBarChangeListener(null);
    }

    void onResponseReceived(String result) {
        if (!result.isEmpty()) {
            parseForecastRequestResponse(result);
            clearViews();
            setViewsDefaults();
            showForecast(0);
            showViews();
            hideKeyboard();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.toast_forecast_response_error), Toast.LENGTH_SHORT).show();
        }
    }

    private void parseForecastRequestResponse(String result) {
        forecastData = ForecastParser.getInstance().parse(result);
    }

    private void clearViews() {
        cityEditText.setText("");
        coordinatesTextView.setText("");
        overallTextView.setText("");
        temperatureTextView.setText("");
        windTextView.setText("");
        rainTextView.setText("");
        pressureTextView.setText("");
        snowTextView.setText("");
        dateTextView.setText("");
        timeTextView.setText("");
    }

    private void setViewsDefaults() {
        cityEditText.setText(forecastData.getCity().printCity());
        coordinatesTextView.setText(forecastData.getCity().printCoordinates());
        timeSpanSeekBar.setMax(forecastData.getForecasts().size() - 1);
        timeSpanSeekBar.setProgress(0);
    }

    private void showForecast(int progress) {
        Forecast forecast = forecastData.getForecasts().get(progress);
        overallTextView.setText(forecast.printDescription());
        temperatureTextView.setText(forecast.printTemperature());
        windTextView.setText(forecast.printWindSpeed());
        rainTextView.setText(forecast.printRainRate());
        pressureTextView.setText(forecast.printPressure());
        snowTextView.setText(forecast.printSnowRate());
        dateTextView.setText(forecast.printDate());
        timeTextView.setText(forecast.printTime());
    }

    private void showViews() {
        coordinatesTextView.setVisibility(View.VISIBLE);
        overallTextView.setVisibility(View.VISIBLE);
        temperatureTextView.setVisibility(View.VISIBLE);
        overallDescriptionTextView.setVisibility(View.VISIBLE);
        temperatureDescriptionTextView.setVisibility(View.VISIBLE);
        windTextView.setVisibility(View.VISIBLE);
        rainTextView.setVisibility(View.VISIBLE);
        windDescriptionTextView.setVisibility(View.VISIBLE);
        rainDescriptionTextView.setVisibility(View.VISIBLE);
        pressureTextView.setVisibility(View.VISIBLE);
        snowTextView.setVisibility(View.VISIBLE);
        pressureDescriptionTextView.setVisibility(View.VISIBLE);
        snowDescriptionTextView.setVisibility(View.VISIBLE);
        dateTextView.setVisibility(View.VISIBLE);
        timeTextView.setVisibility(View.VISIBLE);
        timeSpanSeekBar.setVisibility(View.VISIBLE);
    }

    private void hideKeyboard() {
        InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (manager != null) {
            manager.hideSoftInputFromWindow(cityEditText.getWindowToken(), 0);
        }
    }
}
