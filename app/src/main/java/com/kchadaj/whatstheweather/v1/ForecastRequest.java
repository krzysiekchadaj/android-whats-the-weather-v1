package com.kchadaj.whatstheweather.v1;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.SoftReference;
import java.net.HttpURLConnection;
import java.net.URL;

class ForecastRequest extends AsyncTask<String, Void, String> {
    private SoftReference<WeatherActivity> activity;

    ForecastRequest(WeatherActivity activity) {
        this.activity = new SoftReference<>(activity);
    }

    @Override
    protected String doInBackground(String... addresses) {
        return downloadForecast(addresses[0]);
    }

    private String downloadForecast(String address) {
        try {
            URL url = new URL(address);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String data = readBuffer(reader);
            reader.close();
            return data;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String readBuffer(BufferedReader reader) throws IOException {
        String line;
        StringBuilder sb = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        WeatherActivity weatherActivity = activity.get();
        if (weatherActivity != null)
            weatherActivity.onResponseReceived(result);
    }
}
